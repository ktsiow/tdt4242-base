let goBackButton;

const fileExtensions = ["jpg", "png", "gif", "jpeg", "JPG", "PNG", "GIF", "JPEG"];
const selectedOpacity = 0.6;

async function retrieveWorkoutImages(id) {
    let workoutData = null;
    let response = await sendRequest("GET", `${HOST}/api/workouts/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve workout data!", data);
        document.body.prepend(alert);
    } else {
        workoutData = await response.json();

        document.getElementById("workout-title").innerHTML = "Workout name: " + workoutData["name"];
        document.getElementById("workout-owner").innerHTML = "Owner: " + workoutData["owner_username"];

        let hasNoImages = getWorkoutDataImgFiles(workoutData.files).length === 0;
        let noImageText = document.querySelector("#no-images-text");

        if(hasNoImages){
            noImageText.classList.remove("hide");
            return;
        }
        noImageText.classList.add("hide");

        const currentImageFileElement = document.querySelector("#current");

        createImageView(workoutData.files, currentImageFileElement);

        const otherImageFileElements = document.querySelectorAll(".imgs img");
        otherImageFileElements[0].style.opacity = selectedOpacity;

        otherImageFileElements.forEach((imageFileElement) => imageFileElement.addEventListener("click", (event) => {
            createAnimation(event, currentImageFileElement, otherImageFileElements)
        }));
    }
    return workoutData;
}

function createImageView(imageFiles, currentImageFileElement) {

    let filesDiv = document.getElementById("img-collection");
    let filesDeleteDiv = document.getElementById("img-collection-delete");

    let fileCounter = 0
    let isFirstImg = true

    for (let file of imageFiles) {

        let isImage = fileExtensions.includes(file.file.split("/")[file.file.split("/").length - 1].split(".")[1]);

        if(isImage){

            let deleteImageButton = createDeleteImageButton(file, fileCounter);
            filesDeleteDiv.appendChild(deleteImageButton);

            let img = document.createElement("img");
            img.src = file.file;
            filesDiv.appendChild(img);

            if(isFirstImg){
                currentImageFileElement.src = file.file;
                isFirstImg = false;
            }
            fileCounter++;
        }
    }
}


function createAnimation(event, currentImageFileElement, otherImageFileElements) {

    //Changes the main image
    currentImageFileElement.src = event.target.src;

    //Adds the fade animation
    currentImageFileElement.classList.add('fade-in')
    setTimeout(() => currentImageFileElement.classList.remove('fade-in'), 500);

    //Sets the opacity of the selected image to 0.4
    otherImageFileElements.forEach((otherImageFileElement) => otherImageFileElement.style.opacity = 1)
    event.target.style.opacity = selectedOpacity;

}

function createDeleteImageButton(file, fileCounter) {

    let deleteImgButton = document.createElement("input");
    deleteImgButton.type = "button";
    deleteImgButton.className = "btn btn-close";
    deleteImgButton.id = file.url.split("/")[file.url.split("/").length - 2];
    deleteImgButton.style.left = `${(fileCounter % 4) * 191}px`;
    deleteImgButton.style.top = `${Math.floor(fileCounter / 4) * 105}px`;
    deleteImgButton.addEventListener('click', () =>
        handleDeleteImgClick(deleteImgButton.id, "DELETE", `Could not delete workout ${deleteImgButton.id}!`)
    );
    return deleteImgButton

}

function getWorkoutDataImgFiles(workoutDataFiles) {

    let imgFiles = [];
    for (let file of workoutDataFiles) {
        if (fileExtensions.includes(file.file.split(".")[1])) {
            imgFiles.push(file);
        }
    }
    return imgFiles;

}

async function validateImgFileType(id) {
    let file = await sendRequest("GET", `${HOST}/api/workout-files/${id}/`);
    let fileData = await file.json();
    let fileType = fileData.file.split("/")[fileData.file.split("/").length - 1].split(".")[1];

    return fileExtensions.includes(fileType);
}

async function handleDeleteImgClick(id, http_keyword, fail_alert_text) {

    if(!validateImgFileType(id)) {
        return;
    }

    let response = await sendRequest(http_keyword, `${HOST}/api/workout-files/${id}/`);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(fail_alert_text, data);
        document.body.prepend(alert);
    } else {
        location.reload();
    }

}

function handleGoBackToWorkoutClick() {
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    window.location.replace(`workout.html?id=${id}`);
}

window.addEventListener("DOMContentLoaded", async () => {

    goBackButton = document.querySelector("#btn-back-workout");
    goBackButton.addEventListener('click', handleGoBackToWorkoutClick);
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    await retrieveWorkoutImages(id);

});