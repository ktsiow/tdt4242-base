let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;

class MuscleGroup {
    constructor(type) {
        this.isValidType = false;
        this.validTypes = ["Legs", "Chest", "Back", "Arms", "Abdomen", "Shoulders"]

        this.type = this.validTypes.includes(type) ? type : undefined;
    };

    setMuscleGroupType = (newType) => {
        this.isValidType = false;

        if(this.validTypes.includes(newType)){
            this.isValidType = true;
            this.type = newType;
        }
        else{
            alert("Invalid muscle group!");
        }

    };

    getMuscleGroupType = () => {
        console.log(this.type, "SWIOEFIWEUFH")
        return this.type;
    }
}

function handleCancelButtonDuringEdit() {
    setReadOnly(true, "#form-exercise");
    document.querySelector("select").setAttribute("disabled", "")
    okButton.className += " hide";
    deleteButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-exercise");
    if (oldFormData.has("name")) form.name.value = oldFormData.get("name");
    if (oldFormData.has("description")) form.description.value = oldFormData.get("description");
    if (oldFormData.has("duration")) form.duration.value = oldFormData.get("duration");
    if (oldFormData.has("calories")) form.calories.value = oldFormData.get("calories");
    if (oldFormData.has("muscleGroup")) form.muscleGroup.value = oldFormData.get("muscleGroup");
    if (oldFormData.has("unit")) form.unit.value = oldFormData.get("unit");
    if (oldFormData.has("step_by_step")) form.unit.value = oldFormData.get("step_by_step");

    oldFormData.delete("name");
    oldFormData.delete("description");
    oldFormData.delete("duration");
    oldFormData.delete("calories");
    oldFormData.delete("muscleGroup");
    oldFormData.delete("unit");
    oldFormData.delete("step_by_step");

}

function handleCancelButtonDuringCreate() {
    window.location.replace("exercises.html");
}

function generateExerciseForm() {
    let form = document.querySelector("#form-exercise");

    let formData = new FormData(form);
    let submitForm = new FormData();

    submitForm.append("name", formData.get('name'));
    submitForm.append("description", formData.get("description"));
    submitForm.append("duration", formData.get("duration"));
    submitForm.append("calories", formData.get("calories"));
    submitForm.append("muscleGroup", formData.get("muscleGroup"));
    submitForm.append("unit", formData.get("unit"));
    submitForm.append("step_by_step", formData.get("step_by_step"));



    // Adds the files
    for (let file of formData.getAll("files")) {
        submitForm.append("files", file);
    }
    return submitForm;
}

async function createExercise() {
    document.querySelector("select").removeAttribute("disabled")

    let submitForm = generateExerciseForm();


    for (let key of submitForm.keys()) {
        console.log(key, submitForm[key]);
    }

    let response = await sendRequest("POST", `${HOST}/api/exercises/`, submitForm, "");

    if (response.ok) {
        window.location.replace("exercises.html");
    } else {
        let data = await response.json();
        let msg = "";
        if (data.files) {
            data.files.forEach((file) => {
                msg += `${file.file}`;
            });
        }
        let alert = createAlert("Could not create new exercise!", data,msg);
        document.body.prepend(alert);
    }
}

function handleEditExerciseButtonClick() {
    setReadOnly(false, "#form-exercise");
    document.querySelector("#inputOwner").readOnly = true;  // owner field should still be readonly

    document.querySelector("select").removeAttribute("disabled")

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");
    deleteButton.className = deleteButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-exercise");
    oldFormData = new FormData(form);
}

function exercisefilesDiv(exerciseData) {
    if (exerciseData.files && exerciseData.files.length > 0) {
      const mediaCarousel = document.querySelector("#mediaCarousel");
      mediaCarousel.classList.remove("hide");

      exerciseData.files.forEach((file, i) => {
        // Indicator button
        const btn = document.createElement("button");
        btn.setAttribute("type", "button");
        btn.setAttribute("data-bs-target", "#mediaCarousel");
        btn.setAttribute("data-bs-slide-to", i);
        btn.setAttribute("aria-label", `Slide ${i}`);
        if (i === 0) {
          btn.setAttribute("class", "active");
          btn.setAttribute("aria-current", "true");
        }
        const carouselIndicator = document.querySelector(".carousel-indicators");
        carouselIndicator.appendChild(btn);

        // Carousel item
        const carouselItem = document.createElement("div");
        carouselItem.classList.add("carousel-item");
        carouselItem.setAttribute("data-bs-interval", "5000")
        carouselItem.setAttribute("style", "height:400px");
        if (i === 0) {
          carouselItem.classList.add("active");
        }
        var media = null;
        if (isImage(file.file)) {
            media = document.createElement("img");

        }
        else if (isVideo(file.file))
        {
            media = document.createElement('video');
            media.autoplay = true;
            media.controls = true;
            media.muted = false;
        }
        // media = document.createElement("img");
        media.classList.add("d-block");
        media.classList.add("w-100");
        media.classList.add("h-100");
        media.classList.add("mx-auto");
        media.classList.add("pb-4");
        media.setAttribute("style", "object-fit: contain");
        media.src = file.file;
        console.log(media)
        carouselItem.appendChild(media);

        const carouselInner = document.querySelector(".carousel-inner");
        carouselInner.appendChild(carouselItem);
      });
    }
  }

function getExtension(file) {
//   return file && file['type'].split('/')[0] === 'image';
    var parts = file.split('.');
    return parts[parts.length - 1];
}
function isImage(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'jpg':
        case 'gif':
        case 'bmp':
        case 'png':
        //etc
        return true;
    }
    return false;
    }

function isVideo(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'm4v':
        case 'avi':
        case 'mpg':
        case 'mp4':
        // etc
        return true;
    }
    return false;
}

async function deleteExercise(id) {
    let response = await sendRequest("DELETE", `${HOST}/api/exercises/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("exercises.html");
    }
}

async function retrieveExercise(id) {
    let exerciseData = null;
    let response = await sendRequest("GET", `${HOST}/api/exercises/${id}/`);

    console.log(response.ok)

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve exercise data!", data);
        document.body.prepend(alert);
    } else {
        document.querySelector("select").removeAttribute("disabled")
        exerciseData = await response.json();
        let form = document.querySelector("#form-exercise");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector
            key !== "muscleGroup" ? selector = `input[name="${key}"], textarea[name="${key}"]` : selector = `select[name=${key}]`
            let input = form.querySelector(selector);
            let newVal = exerciseData[key];
            // input.value = newVal;

            if (key != "files") {
                input.value = newVal;
            }
        }
        document.querySelector("select").setAttribute("disabled", "")

        // files
        let filesDiv = document.querySelector("#uploaded-files");
        console.log(exerciseData);
        for (let file of exerciseData.files) {
            let a = document.createElement("a");
            a.href = file.file;
            let pathArray = file.file.split("/");
            a.text = pathArray[pathArray.length - 1];
            a.className = "me-2";
            console.log(file)
            filesDiv.appendChild(a);
        }
        exercisefilesDiv(exerciseData);
    }
    return exerciseData;
}

async function updateExercise(id) {
    let form = document.querySelector("#form-exercise");
    let formData = new FormData(form);

    let muscleGroupSelector = document.querySelector("select")
    muscleGroupSelector.removeAttribute("disabled")

    let selectedMuscleGroup = new MuscleGroup(formData.get("muscleGroup"));

    // let body = {"name": formData.get("name"),
    //             "description": formData.get("description"),
    //             "duration": formData.get("duration"),
    //             "calories": formData.get("calories"),
    //             "muscleGroup": selectedMuscleGroup.getMuscleGroupType(),
    //             "unit": formData.get("unit")};

    // let response = await sendRequest("PUT", `${HOST}/api/exercises/${id}/`, body);

    // let submitForm = generateExerciseForm();
    let submitForm = new FormData();
    // let submitTrial = new FormData();

    submitForm.append("name", formData.get('name'));
    submitForm.append("description", formData.get("description"));
    submitForm.append("duration", formData.get("duration"));
    submitForm.append("calories", formData.get("calories"));
    submitForm.append("muscleGroup", selectedMuscleGroup.getMuscleGroupType());
    submitForm.append("unit", formData.get("unit"));
    submitForm.append("step_by_step", formData.get("step_by_step"));

    // submitTrial.append("id",id);
    // Adds the files
    for (let file of formData.getAll("files")) {
        submitForm.append("files", file);
        // submitTrial.append("file",file);
        console.log(file);
    }

    for (let key of submitForm.keys()) {
        console.log(key, submitForm[key]);
    }


    let response = await sendRequest("PUT", `${HOST}/api/exercises/${id}/`, submitForm, "");

    // let response1 = await sendRequest("POST", `${HOST}/api/exercise-files/`, submitForm, "");

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        muscleGroupSelector.setAttribute("disabled", "")
        // duplicate code from handleCancelButtonDuringEdit
        // you should refactor this
        setReadOnly(true, "#form-exercise");
        okButton.className += " hide";
        deleteButton.className += " hide";
        cancelButton.className += " hide";
        editButton.className = editButton.className.replace(" hide", "");
    
        cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);
        
        oldFormData.delete("name");
        oldFormData.delete("description");
        oldFormData.delete("duration");
        oldFormData.delete("calories");
        oldFormData.delete("muscleGroup");
        oldFormData.delete("unit");
        oldFormData.delete("step_by_step");
    }
}

window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-exercise");
    okButton = document.querySelector("#btn-ok-exercise");
    deleteButton = document.querySelector("#btn-delete-exercise");
    editButton = document.querySelector("#btn-edit-exercise");
    oldFormData = null;

    const urlParams = new URLSearchParams(window.location.search);
    let currentUser = await getCurrentUser();

    // view/edit
    if (urlParams.has('id')) {
        const exerciseId = urlParams.get('id');
        let exerciseData = await retrieveExercise(exerciseId);

        if (exerciseData['owner']== currentUser.url){
            editButton.addEventListener("click", handleEditExerciseButtonClick);
            deleteButton.addEventListener("click", (async (id) => await deleteExercise(id)).bind(undefined, exerciseId));
            okButton.addEventListener("click", (async (id) => await updateExercise(id)).bind(undefined, exerciseId));


        }


    } 
    //create
    else {
        let ownerInput = document.querySelector("#inputOwner");
        ownerInput.value = currentUser.username;
        setReadOnly(false, "#form-exercise");
        ownerInput.readOnly = !ownerInput.readOnly;

        editButton.className += " hide";
        okButton.className = okButton.className.replace(" hide", "");
        cancelButton.className = cancelButton.className.replace(" hide", "");

        okButton.addEventListener("click", async () => await createExercise());
        cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
    }
});