// register.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
var baseUrl = 'localhost:9090'

describe("Username Boundary Test ", function () {
    

    beforeEach(() => {

        cy.visit(baseUrl+"/register.html");
        cy.wait(4000);
        cy.get('form');

        cy.get('input[name="email"]').type("email@email.com").should("have.value", "email@email.com");
        cy.get('input[name="password"]').type("password");
        cy.get('input[name="password1"]').type("password");
        cy.get('input[name="phone_number"]').type("47563821")
        cy.get('input[name="country"]').type("Norway").should("have.value", "Norway");
        cy.get('input[name="city"]').type("Trondheim").should("have.value", "Trondheim");
        cy.get('input[name="street_address"]').type("Trondheim").should("have.value", "Trondheim");
        
    });

    it("Checks empty username", function () {
        
        cy.get("#btn-create-account").click();
        cy.contains("Registration failed!");
        cy.contains("This field may not be blank.");

    });

    it("Checks if overlimit username is possible", function () {
        cy.get('input[name="username"]').type("Hei".repeat(51)).should("have.value", "Hei".repeat(51));
        cy.get("#btn-create-account").click();
        cy.contains("Registration failed!");
        cy.contains("Ensure this field has no more than 150 characters");

    });

    it("Checks if long username works", function () {
        cy.get('input[name="username"]').type("Hei".repeat(50)).should("have.value", "Hei".repeat(50));
        cy.get("#btn-create-account").click();
        cy.url().should('include', baseUrl+'/workouts.html');

        cy.contains("Log out");
        cy.contains("SecFit");
    });

    it("Checks if min username works", function () {
        cy.get('input[name="username"]').type("H").should("have.value", "H");
        cy.get("#btn-create-account").click();

        cy.url().should('include', baseUrl+'/workouts.html');

        cy.contains("Log out");
        cy.contains("SecFit");
    });

});


describe("Email Boundary Test", function () {
    
    

    beforeEach(() => {
        const uuid = () => Cypress._.random(0, 1e6);
        const id = uuid();
        const testname = `testname${id}`;
        
        cy.visit(baseUrl+"/register.html");
        cy.wait(4000);
        cy.get('form');

        cy.get('input[name="username"]').type(testname);
        cy.get('input[name="password"]').type("password");
        cy.get('input[name="password1"]').type("password");
        cy.get('input[name="phone_number"]').type("47563821")
        cy.get('input[name="country"]').type("Norway").should("have.value", "Norway");
        cy.get('input[name="city"]').type("Trondheim").should("have.value", "Trondheim");
        cy.get('input[name="street_address"]').type("Trondheim").should("have.value", "Trondheim");
        
    });

    // have no overlimit for email
    // it("Checks if overlimit email is possible", function () {
    //     cy.get('input[name="email"]').type("email".repeat(24)+"@email").should("have.value", "email".repeat(24)+"@email.no");
    //     cy.get("#btn-create-account").click();
    //     cy.contains("Registration failed!");
    //     cy.contains("Ensure this field has no more than 150 characters");

    // });

    it("Checks if long email works", function () {
        cy.get('input[name="email"]').type("email".repeat(18)+"@email.no").should("have.value", "email".repeat(18)+"@email.no");
        cy.wait(1000);
        cy.get("#btn-create-account").click();
        cy.url().should('include', baseUrl+'/workouts.html');

        cy.contains("Log out");
        cy.contains("SecFit");
    });

    it("Checks if invalid email works", function () {

        cy.get('input[name="email"]').type(".,#@").should("have.value", ".,#@");
        cy.wait(1000);
        cy.get("#btn-create-account").click();

        cy.contains("Registration failed!");
    });

});

describe("Phone Boundary Test", function () {


    beforeEach(() => {
        const uuid = () => Cypress._.random(0, 1e6);
        const id = uuid();
        const testname = `testname${id}`;
        
        cy.visit(baseUrl+"/register.html");
        cy.wait(4000);
        cy.get('form');
        

        cy.get('input[name="username"]').type(testname);
        cy.get('input[name="password"]').type("password");
        cy.get('input[name="password1"]').type("password");
        cy.get('input[name="email"]').type("email@email.com")
        cy.get('input[name="country"]').type("Norway").should("have.value", "Norway");
        cy.get('input[name="city"]').type("Trondheim").should("have.value", "Trondheim");
        cy.get('input[name="street_address"]').type("Trondheim").should("have.value", "Trondheim");
    });

    it("Checks empty number", function () {
        cy.get("#btn-create-account").click();
        cy.url().should('include', baseUrl+'/workouts.html');

        cy.contains("Log out");
        cy.contains("SecFit");
        

    });

    it("Checks if overlimit number is possible", function () {
        cy.get('input[name="phone_number"]').type("4".repeat(51)).should("have.value", "4".repeat(51));
        cy.wait(1000);
        cy.get("#btn-create-account").click();
        cy.contains("Registration failed!");
        cy.contains("Ensure this field has no more than 50 characters");

    });

    it("Checks if long number works", function () {
        cy.get('input[name="phone_number"]').type("4".repeat(50)).should("have.value", "4".repeat(50));
        cy.wait(1000);
        cy.get("#btn-create-account").click();
        cy.url().should('include', baseUrl+'/workouts.html');

        cy.contains("Log out");
        cy.contains("SecFit");
    });


    it("Checks if min number works", function () {

        cy.get('input[name="phone_number"]').type("45".repeat(4)).should("have.value", "45".repeat(4));
        cy.wait(1000);
        cy.get("#btn-create-account").click();

        cy.url().should('include', baseUrl+'/workouts.html');

        cy.contains("Log out");
        cy.contains("SecFit");
    });




});