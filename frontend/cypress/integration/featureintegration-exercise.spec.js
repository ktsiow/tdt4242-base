// register.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
var baseUrl = 'localhost:9090'

describe("Feature Integration Tests", function () {

    var counter =1

    beforeEach(() => {
        // counter +=1

        cy.visit(baseUrl+"/login.html");

        //using testAccount created in exercise.spec.js
        cy.get('input[name="username"]').type("testUser");
        cy.get('input[name="password"]').type("password");

        cy.get("#btn-login").click();
        cy.wait(1000); //include this wait time to not cause any errors

        
        
    });

    it("Should be able to create an exercise with Step-by-step Description", function () {
        cy.visit(baseUrl+"/exercise.html");


        cy.get('#inputName').type("Test Step-by-step Exercise");
        cy.get('#inputDescription').type("hello");
        cy.get('#inputDuration').type("123");
        cy.get('#inputCalories').type("45");
        cy.get('#inputUnit').type("45 reps, 15 mins");
        
        cy.get("#inputStepbyStep").type("This is a sample description");
        cy.get("#btn-ok-exercise").click();
        cy.wait(1000);
        cy.url().should("include", "/exercises.html");

    });

    it("Should be able to view an exercise with Step-by-step Description", function () {
        cy.visit(baseUrl+"/exercises.html"); //exercise preloaded with images and videos. 

        cy.contains("Test Step-by-step Exercise").click();
        cy.wait(1000);
        cy.get("#inputStepbyStep").should('have.value', "This is a sample description");

        

    });

    it("Should be able to create an exercise with images/videos", function () {
        cy.visit(baseUrl+"/exercise.html");

        cy.get('#inputName').type("Test Image Upload Exercise");
        cy.get('#inputDescription').type("Testing the Image Upload for an exercise");
        cy.get('#inputDuration').type("123");
        cy.get('#inputCalories').type("45");
        cy.get('#inputUnit').type("45 reps, 15 mins");
        
        //Uploading of test files for file testing
        cy.get("input[type=file]").attachFile(["testImage.jpg","The Push-Up.mp4"]);
        cy.get("#btn-ok-exercise").click();
        cy.wait(1000);
        cy.url().should("include", "/exercises.html", {timeout: 10000});

        cy.contains("Test Image Upload Exercise");

    });

    it("Should be able to view an exercise with images/videos", function () {
        cy.visit(baseUrl+"/exercises.html"); //exercise preloaded with images and videos. 

        cy.contains("Test Image Upload Exercise").click();
        cy.get("img").should('have.attr','src'); //check image upload is present
        cy.get("#carouselNext").click();
        cy.get("video").should('have.attr','src'); //check video upload is present
        

        

    });

});



