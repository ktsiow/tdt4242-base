
var baseUrl = "localhost:9090"

describe("Black-box testing of FR5 - workouts", () => {

    before(() => { //setup test environment
        //register a test user for testing.
        cy.visit(baseUrl+"/register.html");
        cy.get('input[name="username"]').type("testUser");
        cy.get('input[name="email"]').type("testUser@email.no");
        cy.get('input[name="password"]').type("password");
        cy.get('input[name="password1"]').type("password");
        cy.get('[name=phone_number]').type('12345678');
        cy.get('[name=country]').type('Norway');
        cy.get('[name=city]').type('Trondheim');
        cy.get('[name=street_address]').type('Moholt');
        
        cy.get('#btn-create-account').click();
        cy.wait(5000);
        // cy.contains('View Workouts');
        cy.get('#btn-logout').click();
        cy.wait(5000);
        //register a test user as coach
        cy.visit(baseUrl+"/register.html");
        cy.get('input[name="username"]').type("coach");
        cy.get('input[name="email"]').type("coach@email.no");
        cy.get('input[name="password"]').type("iamcoach");
        cy.get('input[name="password1"]').type("iamcoach");
        cy.get('[name=phone_number]').type('87654321');
        cy.get('[name=country]').type('Norway');
        cy.get('[name=city]').type('Trondheim');
        cy.get('[name=street_address]').type('Moholt');
        cy.get('#btn-create-account').click();
        cy.wait(5000);
        cy.get('#btn-logout').click();
        cy.wait(5000);
        //register a test user as athlete
        cy.visit(baseUrl+"/register.html");
        cy.get('input[name="username"]').type("athlete");
        cy.get('input[name="email"]').type("athlete@email.no");
        cy.get('input[name="password"]').type("iamathlete");
        cy.get('input[name="password1"]').type("iamathlete");
        cy.get('[name=phone_number]').type('86756453');
        cy.get('[name=country]').type('Norway');
        cy.get('[name=city]').type('Trondheim');
        cy.get('[name=street_address]').type('Moholt');
        cy.get('#btn-create-account').click();
        cy.wait(1000);
        cy.get('#btn-logout').click();
        cy.wait(5000);
      });

      it('should be able to create relationship with coach and athlete', function () {
        cy.visit(baseUrl+"/login.html");

        // using coach account created
        cy.get('input[name="username"]').type("coach");
        cy.get('input[name="password"]').type("iamcoach");

        cy.get("#btn-login").click();
        cy.wait(1000);
        cy.visit(baseUrl+"/myathletes.html");
        cy.get('input[name="athlete"]').type("athlete");
        cy.get('#button-submit-roster').click();
        cy.wait(1000);
        cy.get('#btn-logout', { timeout: 5000 }).click();
        cy.wait(5000);

        cy.visit(baseUrl+"/login.html");
        // using coach account created
        cy.get('input[name="username"]').type("athlete");
        cy.get('input[name="password"]').type("iamathlete");

        cy.get("#btn-login").click();
        cy.wait(1000);
        cy.visit(baseUrl+"/mycoach.html");
        cy.get('button[type=button]').should('have.class','btn btn-success').contains('Accept').click();
        cy.wait(1000);
        cy.get('#input-coach').should('have.value','coach');
        cy.wait(1000);
        cy.get('#btn-logout', { timeout: 5000 }).click();
        cy.wait(5000);
        
    });
      it('should be able to create public workout that is visible for all users', function () {
        cy.visit(baseUrl+"/login.html");

        //using testAccount created
        cy.get('input[name="username"]').type("testUser");
        cy.get('input[name="password"]').type("password");

        cy.get("#btn-login").click();
        cy.wait(1000);
        cy.get("#btn-create-workout").click();
        cy.wait(1000);
        cy.get('#inputName').type("Testing Public Workout");
        cy.get('#inputDateTime').type("2022-03-10T09:00");
        cy.get('#inputVisibility').select("PU");
        cy.get('#inputVisibility').should("have.value","PU");
        cy.get('#inputNotes').type("Testing creation of Public Workout");
        cy.get("input[type=file]").attachFile("testImage.jpg");

        cy.get("#btn-ok-workout").click();
        cy.wait(5000);
        cy.url().should('include', "/workouts.html");
        cy.contains("Testing Public Workout");
        cy.wait(1000);
        cy.get('#btn-logout', { timeout: 5000 }).click();
        cy.wait(5000);
    });

    it('Testing that public workout is visible for coach', function () {
        cy.visit(baseUrl+"/login.html");

        cy.get('input[name="username"]').type("coach");
        cy.get('input[name="password"]').type("iamcoach");

        cy.get("#btn-login").click();
        cy.wait(1000);

        cy.contains("Public Workouts").click();
        cy.wait(1000);


        cy.contains("Testing Public Workout");
        cy.contains("testUser");
        cy.wait(1000);
        cy.get('#btn-logout', { timeout: 5000 }).click();
        cy.wait(1000);
    });

    it('Testing that public workout is visible for athlete', function () {
        cy.visit(baseUrl+"/login.html");

        cy.get('input[name="username"]').type("athlete");
        cy.get('input[name="password"]').type("iamathlete");

        cy.get("#btn-login").click();
        cy.wait(1000);
        cy.contains("Public Workouts").click();

        cy.contains("Testing Public Workout");
        cy.contains("testUser");
        cy.wait(1000);
        cy.get('#btn-logout', { timeout: 5000 }).click();
        cy.wait(1000);
    });

    it('should be able to create athlete workout that is visible for coach', function () {
        cy.visit(baseUrl+"/login.html");

        //using testAccount created
        cy.get('input[name="username"]').type("athlete");
        cy.get('input[name="password"]').type("iamathlete");

        cy.get("#btn-login").click();
        cy.wait(1000);
        cy.get("#btn-create-workout").click();
        cy.get('#inputName').type("Testing Athlete to Coach Workout");
        cy.get('#inputDateTime').type("2022-03-10T09:00");
        cy.get('#inputVisibility').select("CO");
        cy.get('#inputVisibility').should("have.value","CO");
        
        cy.get('#inputNotes').type("Testing creation of Athlete to Coach Workout");

        cy.get("#btn-ok-workout").click();
        cy.wait(1000);
        cy.url().should('include', "/workouts.html");
        
        cy.get('#btn-logout', { timeout: 5000 }).click();
    });

    it('Testing that athlete workouts shows up in "Athlete Workouts"', function () {
        cy.visit(baseUrl+"/login.html");

        cy.get('input[name="username"]').type("coach");
        cy.get('input[name="password"]').type("iamcoach");

        cy.get("#btn-login").click();
        cy.wait(1000);
        cy.contains("Athlete Workouts").click();
        cy.wait(1000);
        cy.contains("Testing Athlete to Coach Workout").should('be.visible');
        cy.contains("athlete").should("be.visible");
        cy.wait(1000);
        cy.get('#btn-logout', { timeout: 5000 }).click();

    });

    it('should have public workouts workout files be present"', function () {
        cy.visit(baseUrl+"/login.html");

        cy.get('input[name="username"]').type("coach");
        cy.get('input[name="password"]').type("iamcoach");

        cy.get("#btn-login").click();
        cy.wait(1000);
        cy.contains("Testing Public Workout").click();
        cy.wait(1000);
        //check if file uploaded is presently displayed in public workout.
        cy.get("#uploaded-files").get('a').should('have.attr','href');

        

    });
})