// register.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
var baseUrl = 'localhost:9090'

describe("Units Boundary Test ", function () {

    var counter =1
    before(() => {
        // //register a test user for testing.
        // cy.visit(baseUrl+"/register.html");
        // cy.get('input[name="username"]').type("testUser");
        // cy.get('input[name="email"]').type("testUser@email.no");
        // cy.get('input[name="password"]').type("password");
        // cy.get('input[name="password1"]').type("password");
        // cy.get('[name=phone_number]').type('12345678');
        // cy.get('[name=country]').type('Norway');
        // cy.get('[name=city]').type('Trondheim');
        // cy.get('[name=street_address]').type('Moholt');
        // cy.get('#btn-create-account').click();
        // cy.wait(1000);
        // cy.get('#btn-logout', { timeout: 5000 }).click()
      })
  
    

    beforeEach(() => {
        counter +=1

        cy.visit(baseUrl+"/login.html");
        
        //using testAccount created
        cy.get('input[name="username"]').type("testUser");
        cy.get('input[name="password"]').type("password");

        cy.get("#btn-login").click();
        cy.wait(1000); //include this wait time to not cause any errors

        cy.visit(baseUrl+"/exercise.html");


        cy.get('#inputName').type("Jump"+counter);
        cy.get('#inputDescription').type("hello");
        cy.get('#inputDuration').type("123");
        cy.get('#inputCalories').type("45");
        
    });

    it("Checks empty units", function () {
        
        cy.get("#btn-ok-exercise").click();
        cy.contains("Could not create new exercise!");
        cy.contains("This field may not be blank.");

    });

    it("Check units alphanumsym", function () {
        cy.get('#inputUnit').type("45 reps, 15 mins");
        cy.get("#btn-ok-exercise").click();
        cy.url().should("include", "/exercises.html");

    });


});

describe("Duration Boundary Test ", function () {

    var counter =1

    beforeEach(() => {
        counter +=1

        cy.visit(baseUrl+"/login.html");

        //using testAccount created
        cy.get('input[name="username"]').type("testUser");
        cy.get('input[name="password"]').type("password");

        cy.get("#btn-login").click();
        cy.wait(1000); //include this wait time to not cause any errors

        cy.visit(baseUrl+"/exercise.html");


        cy.get('#inputName').type("Jump"+counter);
        cy.get('#inputDescription').type("hello");
        
        cy.get('#inputUnit').type("45 reps, 15 mins");
        cy.get('#inputCalories').type("45");
        
    });

    it("Checks empty duration", function () {
        
        cy.get("#btn-ok-exercise").click();
        cy.contains("Could not create new exercise!");
        cy.contains("A valid integer is required.");

    });

    it("Check negative duration", function () {
        cy.get('#inputDuration').type("-123");
        cy.get("#btn-ok-exercise").click();
        cy.url().should("include", "/exercises.html");

    });


});

describe("Calories Boundary Test ", function () {

    var counter =1

    beforeEach(() => {
        counter +=1

        cy.visit(baseUrl+"/login.html");

        //using testAccount created
        cy.get('input[name="username"]').type("testUser");
        cy.get('input[name="password"]').type("password");

        cy.get("#btn-login").click();
        cy.wait(1000); //include this wait time to not cause any errors

        cy.visit(baseUrl+"/exercise.html");


        cy.get('#inputName').type("Jump"+counter);
        cy.get('#inputDescription').type("hello");
        
        cy.get('#inputUnit').type("45 reps, 15 mins");
        cy.get('#inputDuration').type("123");
        
    });

    it("Checks empty calories", function () {
        
        cy.get("#btn-ok-exercise").click();
        cy.contains("Could not create new exercise!");
        cy.contains("A valid integer is required.");

    });

    it("Check negative duration", function () {
        cy.get('#inputCalories').type("45");
        cy.get("#btn-ok-exercise").click();
        cy.url().should("include", "/exercises.html");

    });


});

