from django.test import TestCase, Client
from users.serializers import (
    UserSerializer
)

from rest_framework.test import APIClient, APITestCase

"""
Tests for UserSerializer in serializers.py - task 1
"""

class UserSerializerTest(TestCase):
    def test_validate_password(self):
        user_serializer_object = UserSerializer()

        expected_password = "mypassword123"

        serialize_password = UserSerializer.validate_password(user_serializer_object, "mypassword123")

        self.assertEqual(serialize_password, expected_password)

    def test_create_user(self):
        user_serializer_object = UserSerializer()

        expected_user_1 = {
            "username": "Espen Hansen",
            "email": "espen@gmail.com",
            "password": "pass123",
            "phone_number": "54234566",
            "country": "Norway",
            "city": "Oslo",
            "street_address": "Veitvetveien 2"
        }

        serialize_user_1 = UserSerializer.create(user_serializer_object, expected_user_1)

        self.assertEqual(expected_user_1["phone_number"], serialize_user_1.phone_number)
        self.assertEqual(expected_user_1["city"], serialize_user_1.city)

        expected_user_2 = {
            "username": "Broren Min",
            "email": "test@test.com",
            "password": "pass12345",
            "phone_number": "54221122",
            "country": "Norge",
            "city": "Trondheim",
            "street_address": "Berg 1"
        }

        serialize_user_2 = UserSerializer.create(user_serializer_object, expected_user_2)

        self.assertNotEqual(serialize_user_2, expected_user_2)


# Create your tests here.


# Boundary tests cases

defaultforRegistration = {
    "username": "bob",
    "email": "email@email.com",
    "password": "password",
    "password1": "password",
    "phone_number": "12345123",
    "country": "Norway",
    "city": "Trondheim",
    "street_address": "Moholt"
}


class UsernameBoundaryTest(TestCase):
    #400 - Bad Request, 201 - Successful creation
    def setUp(self):
        self.client = Client()


    
    def test_username_empty(self):
        defaultforRegistration["username"]=''
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 400)

    
    def test_username_overlimit(self):
        defaultforRegistration["username"]= "Hei" * 51
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 400)

        
    def test_username_max(self):
        defaultforRegistration["username"]= "Hei" * 50
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_username_min(self):
        defaultforRegistration["username"]= "H"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_username_alphanum(self):
        defaultforRegistration["username"]= "Hei123"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_username_symbols(self):
        defaultforRegistration["username"]= "Hei-+@."
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_username_invalidsymbols(self):
        defaultforRegistration["username"]= "Hei!*&"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 400)

class EmailBoundaryTest(TestCase):
    def setUp(self):
        self.client = Client()


    
    def test_email_alphanum(self):
        defaultforRegistration["email"]= "Hei123@mail.no"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_email_valid(self):
        defaultforRegistration["email"]= "Hei@email.no"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_email_invalid(self):
        invalid_emails = ["23jrrj","324rjw@", ".,#", 'Hei@no']
        for email in invalid_emails:
            defaultforRegistration["email"]= email
            response = self.client.post('/api/users/', defaultforRegistration)
            self.assertEqual(response.status_code, 400)

class PasswordBoundaryTest(TestCase):
    def setUp(self):
        self.client = Client()

    
    def test_password_empty(self):
        defaultforRegistration["password"]= ""
        defaultforRegistration["password1"]= ""
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 400)

    
    def test_password_3digits(self):
        defaultforRegistration["password"]= "hei"
        defaultforRegistration["password1"]= "hei"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_password_min(self):
        defaultforRegistration["password"]= "5"*5
        defaultforRegistration["password1"]= "5" *5
        
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_password_valid(self):
        defaultforRegistration["password"]= "Pass!123"
        defaultforRegistration["password1"]= "Pass!123"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_password_invalid(self):
        defaultforRegistration["password"]= "Pass1"
        defaultforRegistration["password1"]= "Pass1234"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 400)

class NumberBoundaryTest(TestCase):
    def setUp(self):
        self.client = Client()

    
    def test_number_empty(self):
        defaultforRegistration["phone_number"]= ""
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_number_overlimit(self):
        defaultforRegistration["phone_number"]= "4" * 51
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 400)


    
    def test_number_min(self):
        defaultforRegistration["phone_number"]= "45" *4
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_number_valid(self):
        defaultforRegistration["phone_number"]= "47234328"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)




class CountryBoundaryTest(TestCase):
    def setUp(self):
        self.client = Client()

    
    def test_country_empty(self):
        defaultforRegistration["country"]= ""
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_country_overlimit(self):
        defaultforRegistration["country"]= "N" * 51
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 400)

    
    def test_country_max(self):
        defaultforRegistration["country"]= "N" * 50
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_country_valid(self):
        defaultforRegistration["country"]= "Norway"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)



class CityBoundaryTest(TestCase):
    def setUp(self):
        self.client = Client()

    
    def test_city_empty(self):
        defaultforRegistration["city"]= ""
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_city_overlimit(self):
        defaultforRegistration["city"]= "N" * 51
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 400)

    
    def test_city_max(self):
        defaultforRegistration["city"]= "N" * 50
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_city_valid(self):
        defaultforRegistration["city"]= "Trondheim"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

class AddressBoundaryTest(TestCase):
    def setUp(self):
        self.client = Client()

    
    def test_address_empty(self):
        defaultforRegistration["street_address"]= ""
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_address_overlimit(self):
        defaultforRegistration["street_address"]= "N" * 51
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 400)

    
    def test_address_max(self):
        defaultforRegistration["street_address"]= "N" * 50
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)

    
    def test_address_valid(self):
        defaultforRegistration["street_address"]= "Moholt"
        response = self.client.post('/api/users/', defaultforRegistration)
        self.assertEqual(response.status_code, 201)


'''
2-way Domain testing
for Registration

'''



class TwoWayDomainTest(APITestCase):
    def setUp(self):

        #Testing data  - valid, overlimit, empty, invalid
        self.usernames = ["user","u"*51,"","Hei!*#"]
        self.emails = ["nice@mail.no","e"*51+"@email.no","","email,@"]
        self.passwords = ["pass","p"*51,""] 
        self.passwords1 = ["pass","p"*51,""] 
        self.phone_numbers = ["47431234","4"*51,""]
        self.countries= ["Norway","N"*51,""]
        self.cities=["Trondheim","T"*51,""]
        self.street_addresses=["Moholt","M"*51,""]


        self.index = [0,1,2,3] #0 - valid, 1 - overlimit, 2 - empty, 3 - invalid

        # Initializing the scenarios that will be tested
        self.tests = [
            #20 combinations for pairwise
            {"username":self.usernames[self.index[0]], "email":self.emails[self.index[0]], "password":self.passwords[self.index[0]], "password1": self.passwords1[self.index[0]], "phone_number":self.phone_numbers[self.index[0]], "country": self.countries[self.index[0]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[0]]},
            {"username":self.usernames[self.index[1]], "email":self.emails[self.index[1]], "password":self.passwords[self.index[1]],  "password1":self.passwords1[self.index[1]], "phone_number":self.phone_numbers[self.index[1]],  "country":self.countries[self.index[1]],  "city":self.cities[self.index[1]], "street_address":self.street_addresses[self.index[0]]},
            {"username":self.usernames[self.index[2]], "email":self.emails[self.index[2]], "password":self.passwords[self.index[2]],  "password1":self.passwords1[self.index[2]], "phone_number":self.phone_numbers[self.index[2]],  "country":self.countries[self.index[2]],  "city":self.cities[self.index[2]], "street_address":self.street_addresses[self.index[0]]},
            {"username":self.usernames[self.index[3]], "email":self.emails[self.index[3]], "password":self.passwords[self.index[2]],  "password1":self.passwords1[self.index[1]], "phone_number":self.phone_numbers[self.index[1]],  "country":self.countries[self.index[0]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[1]]},
            {"username":self.usernames[self.index[3]], "email":self.emails[self.index[2]], "password":self.passwords[self.index[0]],  "password1":self.passwords1[self.index[2]], "phone_number":self.phone_numbers[self.index[0]],  "country":self.countries[self.index[1]],  "city":self.cities[self.index[1]], "street_address":self.street_addresses[self.index[2]]},
            {"username":self.usernames[self.index[0]], "email":self.emails[self.index[3]], "password":self.passwords[self.index[1]],  "password1":self.passwords1[self.index[0]], "phone_number":self.phone_numbers[self.index[2]],  "country":self.countries[self.index[2]],  "city":self.cities[self.index[1]], "street_address":self.street_addresses[self.index[1]]},
            {"username":self.usernames[self.index[1]], "email":self.emails[self.index[0]], "password":self.passwords[self.index[0]],  "password1":self.passwords1[self.index[1]], "phone_number":self.phone_numbers[self.index[1]],  "country":self.countries[self.index[2]],  "city":self.cities[self.index[2]], "street_address":self.street_addresses[self.index[2]]},
            {"username":self.usernames[self.index[2]], "email":self.emails[self.index[1]], "password":self.passwords[self.index[1]],  "password1":self.passwords1[self.index[2]], "phone_number":self.phone_numbers[self.index[2]],  "country":self.countries[self.index[0]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[2]]},
            {"username":self.usernames[self.index[0]], "email":self.emails[self.index[1]], "password":self.passwords[self.index[2]],  "password1":self.passwords1[self.index[0]], "phone_number":self.phone_numbers[self.index[0]],  "country":self.countries[self.index[1]],  "city":self.cities[self.index[2]], "street_address":self.street_addresses[self.index[1]]},
            {"username":self.usernames[self.index[2]], "email":self.emails[self.index[0]], "password":self.passwords[self.index[0]],  "password1":self.passwords1[self.index[1]], "phone_number":self.phone_numbers[self.index[2]],  "country":self.countries[self.index[1]],  "city":self.cities[self.index[1]], "street_address":self.street_addresses[self.index[1]]},
            {"username":self.usernames[self.index[1]], "email":self.emails[self.index[2]], "password":self.passwords[self.index[1]],  "password1":self.passwords1[self.index[0]], "phone_number":self.phone_numbers[self.index[0]],  "country":self.countries[self.index[0]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[1]]},
            {"username":self.usernames[self.index[3]], "email":self.emails[self.index[3]], "password":self.passwords[self.index[1]],  "password1":self.passwords1[self.index[0]], "phone_number":self.phone_numbers[self.index[0]],  "country":self.countries[self.index[2]],  "city":self.cities[self.index[2]], "street_address":self.street_addresses[self.index[0]]},
            {"username":self.usernames[self.index[2]], "email":self.emails[self.index[3]], "password":self.passwords[self.index[0]],  "password1":self.passwords1[self.index[0]], "phone_number":self.phone_numbers[self.index[1]],  "country":self.countries[self.index[1]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[2]]},
            {"username":self.usernames[self.index[0]], "email":self.emails[self.index[2]], "password":self.passwords[self.index[2]],  "password1":self.passwords1[self.index[1]], "phone_number":self.phone_numbers[self.index[1]],  "country":self.countries[self.index[0]],  "city":self.cities[self.index[1]], "street_address":self.street_addresses[self.index[2]]},
            {"username":self.usernames[self.index[1]], "email":self.emails[self.index[0]], "password":self.passwords[self.index[2]],  "password1":self.passwords1[self.index[2]], "phone_number":self.phone_numbers[self.index[1]],  "country":self.countries[self.index[2]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[1]]},
            {"username":self.usernames[self.index[2]], "email":self.emails[self.index[1]], "password":self.passwords[self.index[0]],  "password1":self.passwords1[self.index[1]], "phone_number":self.phone_numbers[self.index[0]],  "country":self.countries[self.index[2]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[0]]},
            {"username":self.usernames[self.index[3]], "email":self.emails[self.index[0]], "password":self.passwords[self.index[1]],  "password1":self.passwords1[self.index[0]], "phone_number":self.phone_numbers[self.index[2]],  "country":self.countries[self.index[0]],  "city":self.cities[self.index[2]], "street_address":self.street_addresses[self.index[0]]},
            {"username":self.usernames[self.index[1]], "email":self.emails[self.index[3]], "password":self.passwords[self.index[0]],  "password1":self.passwords1[self.index[2]], "phone_number":self.phone_numbers[self.index[2]],  "country":self.countries[self.index[0]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[0]]},
            {"username":self.usernames[self.index[3]], "email":self.emails[self.index[1]], "password":self.passwords[self.index[0]],  "password1":self.passwords1[self.index[0]], "phone_number":self.phone_numbers[self.index[0]],  "country":self.countries[self.index[0]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[0]]},
            {"username":self.usernames[self.index[0]], "email":self.emails[self.index[0]], "password":self.passwords[self.index[0]],  "password1":self.passwords1[self.index[2]], "phone_number":self.phone_numbers[self.index[0]],  "country":self.countries[self.index[0]],  "city":self.cities[self.index[0]], "street_address":self.street_addresses[self.index[0]]},
    
        ]

        
        self.expectedResponse = [201,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400,400]
        self.counter = 0
    
    
    def test_case(self):
         for test in self.tests:
            response = self.client.post('/api/users/', test)
            self.assertEqual(response.status_code, self.expectedResponse[self.counter])
            self.counter+=1