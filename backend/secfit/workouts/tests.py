from django.test import TestCase, RequestFactory
from users.models import User
from workouts.models import Workout, ExerciseInstance, Exercise
from workouts.permissions import (
    IsOwner,
    IsOwnerOfWorkout,
    IsCoachAndVisibleToCoach,
    IsCoachOfWorkoutAndVisibleToCoach,
    IsPublic,
    IsWorkoutPublic,
    IsReadOnly
)

from rest_framework.test import APIClient, APITestCase

from workouts.models import Exercise,ExerciseFile
from users.models import User
"""
Tests for permissions.py - task 1
"""


class IsOwnerTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username="tommy")
        self.workout_object = Workout.objects.create(name="workout1", date="2016-08-06 07:00Z", notes="my notes",
                                                     owner=self.user,
                                                     visibility="PU")

        self.is_owner_test = IsOwner()

    def test_has_object_permission(self):
        request = self.factory.get('/')
        # logged-in user by setting request.user manually.
        request.user = self.user

        permission = self.is_owner_test.has_object_permission(request, None, self.workout_object)
        self.assertEqual(permission, True)


class IsOwnerOfWorkoutTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username="tommy")
        self.workout_object = Workout.objects.create(name="workout1", date="2016-08-06 07:00Z", notes="my notes",
                                                     owner=self.user,
                                                     visibility="PU")

        self.exercise_object = Exercise.objects.create(name="ex1", description="workout", owner=self.user)
        self.exerciseInstance = ExerciseInstance.objects.create(workout=self.workout_object,
                                                                exercise=self.exercise_object,
                                                                sets=5,
                                                                number=6)

        self.is_owner_of_workout_object = IsOwnerOfWorkout()

    def test_has_permission(self):
        request = self.factory.post("/workout")
        request.user = self.user
        request.data = {
            "workout": '/workout/1/'
        }

        permission = self.is_owner_of_workout_object.has_permission(request, None)
        self.assertEqual(permission, True)

        request.data = {
            "exercise": '/exercise/2/'
        }

        permission = self.is_owner_of_workout_object.has_permission(request, None)
        self.assertEqual(permission, False)

        request = self.factory.get("/workout")
        permission = self.is_owner_of_workout_object.has_permission(request, None)
        self.assertEqual(permission, True)

    def test_has_object_permission(self):
        request = self.factory.get("/workout")
        request.user = self.user

        permission = self.is_owner_of_workout_object.has_object_permission(request, None, self.exerciseInstance)
        self.assertEqual(permission, True)


class IsCoachAndVisibleToCoachTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username="tommy")
        self.user1 = User.objects.create_user(username="bob", coach=self.user)
        self.workout_object = Workout.objects.create(name="workout1", date="2016-08-06 07:00Z", notes="my notes",
                                                     owner=self.user,
                                                     visibility="PU")
        self.is_coach_and_visible_to_coach_object = IsCoachAndVisibleToCoach()

    def test_has_object_permission(self):
        request = self.factory.get("/workout")
        request.user = self.user

        permission = self.is_coach_and_visible_to_coach_object.has_object_permission(request, None, self.workout_object)
        self.assertEqual(permission, False)


class IsCoachOfWorkoutAndVisibleToCoachTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username="tommy")
        self.user1 = User.objects.create_user(username="bob", coach=self.user)
        self.workout_object = Workout.objects.create(name="workout1", date="2016-08-06 07:00Z", notes="my notes",
                                                     owner=self.user,
                                                     visibility="PU")
        self.exercise_object = Exercise.objects.create(name="ex1", description="workout", owner=self.user)
        self.exerciseInstance = ExerciseInstance.objects.create(workout=self.workout_object,
                                                                exercise=self.exercise_object,
                                                                sets=5,
                                                                number=6)
        self.is_coach_of_workout_and_visible_to_coach_object = IsCoachOfWorkoutAndVisibleToCoach()

    def test_has_object_permission(self):
        request = self.factory.get("/workout")
        request.user = self.user

        permission = self.is_coach_of_workout_and_visible_to_coach_object.has_object_permission(request, None, self.exerciseInstance)
        self.assertEqual(permission, False)


class IsPublicTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username="tommy")
        self.workout_object = Workout.objects.create(name="workout1", date="2016-08-06 07:00Z", notes="my notes",
                                                     owner=self.user,
                                                     visibility="PU")
        self.is_public_object = IsPublic()

    def test_has_object_permission(self):
        request = self.factory.get("/workout")
        request.user = self.user

        permission = self.is_public_object.has_object_permission(request, None, self.workout_object)
        self.assertEqual(permission, True)


class IsWorkoutPublicTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username="tommy")
        self.workout_object = Workout.objects.create(name="workout1", date="2016-08-06 07:00Z", notes="my notes",
                                                     owner=self.user,
                                                     visibility="PU")
        self.exercise_object = Exercise.objects.create(name="ex1", description="workout", owner=self.user,)
        self.exerciseInstance = ExerciseInstance.objects.create(workout=self.workout_object,
                                                                exercise=self.exercise_object,
                                                                sets=5,
                                                                number=6)
        self.is_workout_public_object = IsWorkoutPublic()

    def test_has_object_permission(self):
        request = self.factory.get("/workout")
        request.user = self.user

        permission = self.is_workout_public_object.has_object_permission(request, None, self.exerciseInstance)
        self.assertEqual(permission, True)


class IsReadOnlyTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username="tommy")
        self.workout_object = Workout.objects.create(name="workout1", date="2016-08-06 07:00Z", notes="my notes",
                                                     owner=self.user,
                                                     visibility="PU")

        self.is_read_only_object = IsReadOnly()

    def test_has_object_permission(self):
        request = self.factory.get("/workout")
        request.user = self.user

        permission = self.is_read_only_object.has_object_permission(request, None, self.workout_object)
        self.assertEqual(permission, True)



#Boundary Test Cases

#testing on except 'Name','Description' and 'Muscle group'
#fields = ["url", "id", "name", "description", "duration", "calories", "muscleGroup", "unit", "step_by_step", "owner","owner_username","files"]
defaultforExercise = {
    "name": "Jump",
    "description": "hello",
    "unit": "15 reps", #reps, time, etc.
    "duration": "60", #expect positive integer
    "calories": "125", #expect positive integer
    "muscleGroup": "Legs"
}

class UnitBoundaryTest(TestCase):
    #400 - Bad Request, 201 - Successful creation
    def setUp(self):
        self.client = APIClient()

        self.user = User.objects.create(username='testuser')
        self.user.set_password("123")
        self.user.save()

        self.client.force_authenticate(self.user)

    
    def test_unit_empty(self):
        defaultforExercise["unit"]=""
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 400)

    def test_unit_overlimit(self):
        defaultforExercise["unit"]= "H"*51
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 400)


class DurationBoundaryTest(TestCase):
    #400 - Bad Request, 201 - Successful creation
    def setUp(self):
        self.client = APIClient()

        self.user = User.objects.create(username='testuser')
        self.user.set_password("123")
        self.user.save()

        self.client.force_authenticate(self.user)

    
    def test_duration_empty(self):
        defaultforExercise["duration"]=''
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 400)

    
    def test_duration_negnumber(self):
        defaultforExercise["duration"]= -15
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 400)
        

    
    def test_duration_symbols(self):
        defaultforExercise["duration"]= "45-+@."
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 400)

    
    def test_duration_words(self):
        defaultforExercise["duration"]= "no time"
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 400)


class CaloriesBoundaryTest(TestCase):
    #400 - Bad Request, 201 - Successful creation
    def setUp(self):
        self.client = APIClient()

        self.user = User.objects.create(username='testuser')
        self.user.set_password("123")
        self.user.save()

        self.client.force_authenticate(self.user)

    
    def test_calories_empty(self):
        defaultforExercise["calories"]=0
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 201)
        
    
    def test_calories_posnumbers(self):
        defaultforExercise["calories"]= "45"
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 201)

    
    def test_calories_symbols(self):
        defaultforExercise["calories"]= "45-+@."
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 400)

    
    def test_calories_words(self):
        defaultforExercise["calories"]= "no time"
        response = self.client.post('/api/exercises/', defaultforExercise)
        self.assertEqual(response.status_code, 400)


# Integration Testing 
# Also with Cypress.
class IntegrationExerciseTest(APITestCase): 
    
    # fields = ["url", "id", "name", "description", "duration", "calories", "muscleGroup", "unit", "step_by_step", "owner","owner_username","files"]
        
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(id=2,username = 'test', email= 'email@email.no',
                phone_number = '43545678', country= 'Norway', city = 'Trondheim', street_address = 'Moholt'
                )
        self.user.save()
        self.createdexercise = Exercise.objects.create(
            id=4, name='Exercise Test', description =" A test exercise",duration='23', calories = '14',
            unit= '23 reps', owner = self.user
        )
        self.createdexercise.save()
        #["url", "id","owner","file", "exercise"]
        self.exercisefiles = ExerciseFile.objects.create(
            id=2, owner = self.user,exercise = self.createdexercise
        )
        self.exercisefiles.save()
        self.client.force_authenticate(user=self.user)
        
    def test_valid_creation_with_step_by_step(self):
        valid_payload = {
            'name': "Valid Payload with Step_by_step",
            'description': "This is a test description",
            'duration': "24",
            'calories': "14",
            'unit': "23 reps",
            'owner': "http://localhost:9090/api/users/2/",
            'step_by_step': "A sample step by step description"
        }
        
        response = self.client.post('/api/exercises/', valid_payload )

        self.assertEquals(response.status_code, 201)

    def test_valid_creation_with_exercise_files(self):

        valid_payload = {
            'name': "Valid Payload with Step_by_step",
            'description': "This is a test description",
            'duration': "24",
            'calories': "14",
            'unit': "23 reps",
            'owner': "http://localhost:9090/api/users/2/",
            'files': []
        }
        
        response = self.client.post('/api/exercises/', valid_payload )

        self.assertEquals(response.status_code, 201)
